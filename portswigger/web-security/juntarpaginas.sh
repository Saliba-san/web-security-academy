#!/bin/bash
classes=$(ls -d */)

[ -f "./notes.md" ] && rm ./notes.md
while IFS= read -r line
do
    echo "$line" | sed -E -e 's/^[[:digit:]]+ -/#/' -e 's/\/$//' >> notes.md
    cat "./$line/README.md" | sed -E 's/^#/##/' >> notes.md
    echo "" >> notes.md
done <<< "$classes"
