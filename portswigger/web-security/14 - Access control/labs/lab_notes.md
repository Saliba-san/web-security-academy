# Lab: Unprotected admin functionality with unpredictable URL
To solve this lab we must find a script element in the page html so that some hidden element is un-hidden by setting it's attribute. What I did was:

1. Grab the HTML of the page, this can be done via devtools or curl
2. grep for "admin" so I could find the setAttribute function and, in turn, find the sensible URL.
