# Access Control Categories
## Vertical Access Controls
Vertical access controls are mechanisms that restrict access to sensitive functionality that is not available to other types of users. So a normal user are restricted from some functionality that is available to the administrator

## Horizontal Access Controls
Horizontal access controls are mechanisms that restrict access to resources to the users who are specifically allowed to access those resources. For example a banking app will allow an user to access a his account but not another user's, even though they are in the same vertical access level.

## Context-dependent access controls
Context-dependent access controls restrict access to functionality and resources based upon the state of the application or the user's interaction with it.

Context-dependent access controls prevent a user performing actions in the wrong order. 

# Broken Access Control
An **access control** is broken when an user is able to perform actions they were not supposed to. Here are some examples:

## Vertical privilege escalation
If a user can gain access to functionality that they are not permitted to access then this is vertical privilege escalation. For example, if a non-administrative user can in fact gain access to an admin page where they can delete user accounts, then this is vertical privilege escalation.

### Unprotected functionality
Vertical privilege escalation arises where an application does not enforce any protection over sensitive functionality.

For example, a website might host sensitive functionality at the following URL:
- https://insecure-website.com/admin

This might be accessible by anyone with the URL, so it can be found either by bruteforcing the location via a wordlist (using something like [subfinder](https://github.com/projectdiscovery/subfinder)) or it's disclosed in a file such as robots.txt in https://insecure-website.com/admin

Sometimes this URL might be a little bit more concealed by a less obvious URL: so called security by obscurity. But simply hiding it with obfuscation doesn't mean it is not obtainable. For instance, take this sensitive URL:
- https://insecure-website.com/administrator-panel-yb556

This might not be directly guessable by an attacker. However, the application might still leak the URL to users. For example, the URL might be disclosed in JavaScript that constructs the user interface based on the user's role: 
```html
<script>
var isAdmin = false;
if (isAdmin) {
	...
	var adminPanelTag = document.createElement('a');
	adminPanelTag.setAttribute('https://insecure-website.com/administrator-panel-yb556');
	adminPanelTag.innerText = 'Admin panel';
	...
}
</script>
```

This script adds a link to the user's UI if they are an admin user. However, the script containing the URL is visible to all users regardless of their role.

### Parameter-based access control methods
Parameter-based **access control** rely on the browser storage to verify the identity of the user in fields like Cookies or Hidden Fields. The main problem with this approach is that the user has control of these parameters, as they are stored on his local machine, making it possible to alter the value of these variables. Which is fundamentally insecure because a user can simply modify the value and gain access to functionality to which they are not authorized, such as administrative functions. 


# Access Control Security Models
An access control security model is a formally defined definition of a set of access control rules that is independent of technology or implementation platform

## Programmatic access control
A matrix of user privileges is stored in the database with the user privileges and the **access control** is in turn applied to based of this matrix

## Directionary access control (DAC)
With **DAC**, access to resources or functions is constrained based upon users or named groups of users. Owners of resources or functions have the ability to assign or delegate access permissions to users. This model is highly granular with access rights defined to an individual resource or function and user. Consequently the model can become very complex to design and manage.

## Mandatory access control (MAC)
**MAC** is similar to **DAC** in the sense that the **access control** is controlled centrally by some system with greater power, so there are no individuals capable of defining access rights to a resource. That is common in military clearence-based systems.

## Role-based access control (RBAC)
With role-based access control, named roles are defined to which access privileges are assigned. Users are then assigned to single or multiple roles. 

RBAC is most effective when there are sufficient roles to properly invoke access controls but not so many as to make the model excessively complex and unwieldy to manage.
